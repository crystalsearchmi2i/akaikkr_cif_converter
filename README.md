# AkaiKKR cif converter version 0.4 #

Copyright 2017 Hiori Kino

This software includes the work that is distributed in the Apache License 2.0.


## sample ##

```
python cif2akaikkr.py ciffile -o outputfilename

optional: 
-o outputfilename, default = kkrinput
```

## requirement ##
* python 2.7
* PyCifRW,
   tested with PyCifRW-4.1.1
* pymatgen
* m_tspace and find_wy, if you use 'litecif' format

for [find_wy](https://github.com/nim-hrkn/find_wy)

for [m_tspace](https://github.com/nim-hrkn/m_tspace)

### installation ###
m_tspace is a library for find_wy. 
Please 'make' on find_wy/ and place the executable file 'write1wy' on the 'NME/' directory. 

## cif format ##
cif format doesn't read _symmetry_Int_Tables_number, but needs _symmetry_equiv_pos_as_xyz, which descript symmetry operations. It isn't easy to prepared them by hand. 

## litecif format ##
write1wy reads _symmetry_Int_Tables_number, and the 'litecif' format is only
```
_cell_length_a 7.71
_cell_length_b 7.71
_cell_length_c 7.71
_cell_angle_alpha 90
_cell_angle_beta 90
_cell_angle_gamma 90
_symmetry_Int_Tables_number 227
loop_
    _atom_site_label
    _atom_site_fract_x
    _atom_site_fract_y
    _atom_site_fract_z
    _atom_site_Wyckoff_symbol
 Fe 0 0 0 c
 Pr 0.375 0.375 0.375  b
```
The origin of the choice is automatically selected. 

## known bug(s) ##
loop_ must be the last section in the lite cif format. 

## problems ##
For Mac OS users:
It is better to use terminal.app to install and run pymatgen.
It is reported that xquartz causes the 'LookupError: unknown encoding' error.

## limitation ##
* stoichiometric structure, no alloy, though it is possible to write alloys. 

## license ###
Copyright 2017 Hiori Kino

This software includes the work that is distributed in the Apache License 2.0.