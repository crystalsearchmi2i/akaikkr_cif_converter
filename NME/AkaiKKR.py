# -*- coding: utf-8 -*-

# Copyright 2017 Hiori Kino, 
# This software includes the work that is distributed in the Apache License 2.0.

import sys
import os
import copy
import NME.core
import NME.tspace 
import NME.liteCif
import numpy as np

import pymatgen.core.periodic_table


class Input:
    """ 
      Akai KKR input structure 
    """

    @staticmethod
    def skipcomment(line,i):
        """
           Akai KKRのcommentをskipする関数。
　　　　　読み込むときにしか使われない。
        """
        while True:
            if len(line[i])==0:
                i+=1
                continue
            if line[i][0].lower()=="c":
                i+=1
            else:
                s=line[i].split()
                if len(s)==0:
                     i+=1
                     continue
                else:
                     return [True,i]
        return [False,0]


    def __init__(self, coordtype="frac",param={}):
        """
        input: coordtype = frac or cart
                param = dictionary, default値から上書きするAkaiKKR parameter

        注意：
        l_max_defaultは原子に対して追加の必要がある。
        現在はlistにあるものはすべて＝２。
        self.l_max_default={"H":2, "Li":2, "O":2, "Mg":2, "Cr":2,"Co":2, "Fe":2, "Ga":2, "As":2, "Y":2}
        """

        self.filename=""
        self.coordtype = coordtype
        self.dic={}
        self.type=[]
        self.atom=[]

        """ keywordのその型の定義"""
        self.field1=[["go",str],["file",str]]
        self.field2 = [ ["brvtyp",str] ]
        self.field3=[["edelt",float],  [  "ewidth",float],    ["reltyp",str], [  "sdftyp",str], [  "magtyp",str],  [ "record",str] ]
        self.field4=[["outtyp",str], [   "bzqlty",int],  [ "maxitr",int], [  "pmix",float] ]
        self.ntyp_field = [["ntyp",int]]
        self.type_field = [ ["type",str], ["ncmp",int], ["rmt",float], ["field",float], ["l_max",int] ]
        self.anclr_field =[  ["anclr",float], ["conc",float] ] 
        self.natm_field = [["natm",int]]
        self.atomicx_field = [ ["atomicx0",str], ["atomicx1",str],["atomicx2",str],["atmtyp",str] ]

        self.l_max_default={"H":2, "Li":2, "O":2, "Mg":2, "Cr":2,"Co":2, "Fe":2, "Ga":2, "As":2, "Y":2}
 
        self.set_defaultparam(0)

        """ paramを上書き"""
        for x in param:
            if x in self.dic:
                print "change param '"+x+"' :", self.dic[x], "-> ",param[x]
                self.dic[x]= param[x]
            else:
                print "Error: typo param",x, param[x],"is set."
                raise Exception("type param")

    def set_defaultparam(self,i=0):
        """ 
         AkaiKKRのdefault parameterをsetする
　　　　input: i
          iの値によりパラメタを切り替える。
        """
        if i==0:
            self.defaultvalue={}
            self.defaultvalue.update({"go":"go", "edelt":0.001, "ewidth":0.8})
            self.defaultvalue.update({"reltyp":"sra", "sdftyp":"mjw"})
            self.defaultvalue.update({"magtyp":"mag", "record":"2nd"})
            self.defaultvalue.update({"outtyp":"update", "bzqlty":2 }) 
            self.defaultvalue.update({"maxitr":50, "pmix":0.01 } )
            for x in self.defaultvalue:
                self.dic[x]= self.defaultvalue[x] 
        else:
            print "not supported, set_param, i=", i
            raise Exception("parameter error")


    @staticmethod
    def makepythonreadable(line):
        """
         AkaiKKR inputを読み込むときにしか用いない。
    　　　　 fortranの一部のformatをpythonで読めるようにする。

        """
        line.replace(","," , ")
        s=line.split()
        t=[]
        for a in s:
            if a==",":
                continue
            else:
                t.append(a)

        return t

    def add_file(self,filename):
        """
         filenameのsetを行う。
        """
        self.dic["file"] = filename

    #def add_lattice(self,brvtyp,a,b,c,alpha,beta,gamma):
    #    """
    #      格子情報をsetする
    #    """
    #    self.dic["brvtyp"]=brvtyp
#
#        self.dic["a"] = a
#        self.dic["b"] = b
#        self.dic["c"] = b
#        self.dic["coa"] = c/a
#        self.dic["boa"] = b/a
#
#        self.dic["alpha"] = alpha
#        self.dic["beta"] = beta
#        self.dic["gamma"] = gamma

    def add_tv(self,brvtyp, tv ):
        """ input 
          brvtyp = ブラベ格子名
          tv = primitive vector 
        """
        self.dic["brvtyp"] =  brvtyp
        self.dic["tv"] = tv 

    @staticmethod
    def anclrFieldHelper(anclr, conc):
         """ 
             anclrとconc用のhelper関数
　　　　　　　　　　　　　anclrとconcはlistで
             dicのlistで渡すのでそのための関数。
         """
         if isinstance(anclr,int) or isinstance(anclr,float):
             z = str(z)
         else: 
             try: 
                 intz = pymatgen.core.periodic_table.Element(anclr).Z
             except:
                 print "uknown element type ("+ anclr +")"
                 raise Exception("uknown element type")
             z = str(intz)
         dic={"anclr":z, "conc":conc}
         return dic

    def add_type(self,type_, anclr_field_list, rmt=0, field=0, l_max=2):
       """
         原子名とanclr fieldを与えてdicに追加する。
       """
       dic={"type":type_,"ncmp":len(anclr_field_list) ,"rmt":rmt, "field":field, "l_max":l_max,
 "anclr_field_list":anclr_field_list}
       self.type.append(dic)
       self.dic["ntyp"] = len(self.type)

    def add_atom(self, x,y,z, type_):
       """
          原子名と原子位置をdicに追加する。
       """
       dic = { "atomicx0": x , "atomicx1":y ,"atomicx2":z ,"atmtyp":type_ }
       self.atom.append(dic)
       self.dic["natm"] = len(self.atom)


    def from_file(self,filename):
        """
          AkaiKKR input fileから情報を読み込む
          fortranではいい加減なformatでも読めるが多言語ではそれは読みにくいのですべてのinputが読めるわけではない。
        """
        self.filename=filename
        with open(self.filename,"r") as f:
            lines=f.readlines()

        newlines=[]
        for x in lines:
            newlines.append(x.rstrip("\n"))
        lines=newlines

        field=0
        found,field=self.skipcomment(lines,field)
        s=self.makepythonreadable(lines[field])
        name=self.field1
        for i in range(len(name)):
            self.dic[name[i][0]]=name[i][1](s[i])
        field = field+ 1

        found,field=self.skipcomment(lines,field)
        s=self.makepythonreadable(lines[field])
        name=self.field2
        for i in range(min(len(name),len(s))):
            self.dic[name[i][0]]=name[i][1](s[i])
        field += 1

        found,field=self.skipcomment(lines,field)
        s=self.makepythonreadable(lines[field])
        name= self.field3
        for i in range(len(name)):
               self.dic[name[i][0]]=name[i][1](s[i])
        field+=1

        found,field=self.skipcomment(lines,field)
        s=self.makepythonreadable(lines[field])
        name=self.field4
        for i in range(len(name)):
               self.dic[name[i][0]]=name[i][1](s[i])
        field+=1


        found,field=self.skipcomment(lines,field)
        s=self.makepythonreadable(lines[field])
        name = self.ntyp_field
        for i in range(len(name)):
               self.dic[name[i][0]]=name[i][1](s[i])
        field+=1

        ntyp = self.dic["ntyp"]
        name = self.type_field
        name2 = self.anclr_field
        for i in range(ntyp):
               found,field=self.skipcomment(lines,field)
               s=self.makepythonreadable(lines[field])
               atomtype={}
               for j in range(len(name)):
                   atomtype[name[j][0]]=name[j][1](s[j])

               field+=1
               found,field=self.skipcomment(lines,field)
               s=self.makepythonreadable(lines[field])
               cmplist = []
               ncmp=atomtype["ncmp"]
               for j in range(ncmp):
                   found,field=self.skipcomment(lines,field)
                   s=self.makepythonreadable(lines[field])
                   cmp1 = {}
                   for k in range( len(name2) ):
                       cmp1[name2[k][0]]=name2[k][1](s[k])
                   field+=1
                   cmplist.append(cmp1)

               atomtype["cmp"]= cmplist
               self.type.append(atomtype)

        found,field=self.skipcomment(lines,field)
        s=self.makepythonreadable(lines[field])
        name = self.natm_field
        for i in range(len(name)):
               self.dic[name[i][0]]=name[i][1](s[i])
        field+=1


        natm = self.dic["natm"]
        name = self.atomicx_field
        atom={}
        for i in range(natm):
               found,field=self.skipcomment(lines,field)
               s=self.makepythonreadable(lines[field])
               atom={}
               for j in range(len(name)):
                   atom[name[j][0]]=name[j][1](s[j])
               self.atom.append(atom)
               field+=1

#    @staticmethod
#    def typeFieldHelper(type_, ncmp,rmt, field, l_max, anclr_field_list):
#          dic={"type":type_,"ncmp":ncmp,"rmt":rmt, "field":field, "l_max":l_max, "anclr_field_list":anclr_field_list}
#          return dic
#    @staticmethod
#    def atomicxFieldHelper(atomicx0, atomicx1,atomicx2,atmtyp):
#        dic={"atomicx0":atomicx0 , "atomicx1":atomicx1 ,"atomicx2":atomicx2 ,"atmtyp":atmtyp }
#        return dic

    def Writer(self):
        """
          dic -> AkaiKKR inputへの変換
          output:
            list of string
        """
        lines=[]
        for name in [self.field1,self.field2]:
            s= [ n[0] for n in name ]
            #print s
            lines.append("c---------------------------------------")
            lines.append("c--- "+" ".join(s) )
            s=[]
            for x,y in name:
                s.append(str(self.dic[x]))
            lines.append(" ".join(s))

        # write r = pritimive vector
        round_n = 6
        for i in range(3):
           lines.append( " ".join([ str(round(self.dic["tv"][i][j],round_n)) for j in range(3) ] )  )

        # write an unit of length
        lines.append("c--- length scale = au")
        lines.append( str(1.0/0.529177) )


        for name in [self.field3,self.field4,self.ntyp_field]:
            s= [ n[0] for n in name ]
            #print s
            lines.append("c---------------------------------------")
            lines.append("c--- "+" ".join(s) )
            s=[]
            for x,y in name:
                s.append(str(self.dic[x]))
            lines.append(" ".join(s))


        s=[]
        for x ,y in self.type_field:
            s.append(x) 
        lines.append("c---------------------------------------")
        lines.append("c--- "+" ".join(s) )
        s=[]
        for x ,y in self.anclr_field:
            s.append(x)
        lines.append("c---------------------------------------")
        lines.append("c---          "+" ".join(s) )


        for t in self.type:
            s=[]
            for x,y in self.type_field:
               s.append(str(t[x]))
            lines.append(" ".join(s))
            print "test t[anclr_field_list]=", t["anclr_field_list"]
            print "test self.anclr_field=", self.anclr_field
            for cmp_ in t["anclr_field_list"]:
               s=[" "*20]
               for x,y in self.anclr_field:
                  s.append(str(cmp_[x]))
               lines.append(" ".join(s))

        s=[]
        for x ,y in self.natm_field:
            s.append(x)
        lines.append("c---------------------------------------")
        lines.append("c--- "+" ".join(s) )

        for name in [self.natm_field]:
            s=[]
            for x,y in name:
                s.append(str(self.dic[x]))
            lines.append(" ".join(s))

        s=[]
        for x ,y in self.atomicx_field:
            s.append(x)
        lines.append("c---------------------------------------")
        lines.append("c--- "+" ".join(s) )

        for atom in self.atom:
            s=[]
            for x,y in self.atomicx_field:
               s.append(str(atom[x]))
            lines.append(" ".join(s))
        
        return lines

    def __str__(self):
        strlist=[]
        strlist.append( self.dic.__str__() )
        for x in self.type:
            strlist.append( x.__str__() )
        for x in self.atom:
            strlist.append( x.__str__() )
        return "\n".join(strlist)

class InputParser():
    """
       cifを読み込み、AkaiKKR input形式に直す。

       元素labelは element_{multiplicity}{WyckoffCharacter}_index となる。
    """
    def __init__(self,datafilename,  coordtype="frac", param={}):
        """
          input: 
            datafilename: AkaiKKRで'go'の行に書くfilename
            coordtype = frac or cart
            param = dic形式、Akai KKRのoptionを記述できる。
        """
        self.datafilename =  datafilename
        self.coordtype = coordtype
        self.param = param 

    def from_liteciffile(self,filename):
        """ 
        litecif fileを読み込んで処理
           tspaceを用いる

        input: filename   litecif format
        """
        self.filename = filename
        litecif = NME.liteCif.Reader(filename)
        print "cif=",litecif
        gen = NME.tspace.Generator(litecif)
    
        print gen.primitivevector
        print gen.conventionalvector
        for ipos,pos in enumerate(gen.atoms):
            print "group",ipos
            for p in pos:
                print p 

        kkr = Input(coordtype=self.coordtype, param=self.param)

        kkr.add_tv( "prv", gen.primitivevector )
        label_wy_list = []
        for ipos,pos in enumerate(gen.atoms):
            element = pos[0][0]
            mul = str(pos[0][1])
            wych = pos[0][2]
            label_wy_list.append( "_".join([element,mul+wych,str(ipos+1)]) )
        for label_wy,pos in zip(label_wy_list,gen.atoms):
            element = pos[0][0]
            kkr.add_type( label_wy, anclr_field_list= [ kkr.anclrFieldHelper(element,100) ] )


        print "coordtype=", kkr.coordtype 
        round_n = 7
        if kkr.coordtype == "frac":

            _axis=["a","b","c"]
            for label_wy,pos in zip(label_wy_list, gen.atoms):
                for p in pos:
                    abcv = NME.core.cfrac2pfrac(gen.conventionalvector, gen.primitivevector, p[3] )
                    abcv = np.array(abcv)[0]
                    v= []
                    for abc,label in zip(abcv, _axis) :
                        v.append(str(round(abc,round_n))+label)
                    kkr.add_atom( v[0], v[1], v[2], label_wy )

        else:

            for label_wy,pos in zip(label_wy_list, gen.atoms):
                for p in pos:
                    xyz = NME.core.frac2cart(gen.conventionalvector, p[3] ) 
                    xyz = np.array(xyz)[0]
                    v= []
                    for abc in xyz:
                        v.append(str(round(abc,round_n)))
                    kkr.add_atom( v[0], v[1], v[2], label_wy )


        self.kkr = kkr



    def from_ciffile(self,filename):
        """ 
        cif fileを読みこんで処理
          pymatgenを用いる。

        input : filename 
        """
        self.filename = filename

        sp = copy.deepcopy(NME.core.StructureParser(self.filename))
        self.dic_lattice = sp.dic_lattice
        self.dic_sites_fix_list = sp.dic_sites_fix_list

        kkr = Input(coordtype=self.coordtype, param=self.param)

        #kkr.add_lattice( newstruc.akaikkr_latticetype, 
        #         newstruc.a, newstruc.b, newstruc.c,
        #         newstruc.alpha, newstruc.beta, newstruc.gamma )
        kkr.add_tv( "prv", self.dic_lattice["matrix"] )

        label_list = []
        for x in self.dic_sites_fix_list:
            label_list.append( x["label"] )
        label_list = list ( set ( label_list ) ) 

        for label_wy in  label_list :
            element = label_wy.split("_")[0]
            kkr.add_type( label_wy, anclr_field_list= [ kkr.anclrFieldHelper(element,100) ] )

        round_n = 7
        if kkr.coordtype == "frac": 

            _axis=["a","b","c"]
            for atom in self.dic_sites_fix_list:
                    v= []
                    for abc,label in zip(atom["abc"], _axis) :
                        v.append(str(round(abc,round_n))+label) 
                    kkr.add_atom( v[0], v[1], v[2], atom["label"] ) 

        else:

            for atom in self.dic_sites_fix_list:
                    v= []
                    for xyz in atom["xyz"]:
                        v.append(str(round(xyz,round_n)))
                    kkr.add_atom( v[0], v[1], v[2], atom["label"] )

        self.kkr = kkr


    def write_file(self, kkrinputfilename):
        """ akai-kkr形式で出力
          input: kkrinputfilename
        """
        kkr = self.kkr 
        kkr.add_file("data/"+self.datafilename)

        #print kkr 
        #print "-----------------"
        writer = kkr.Writer()
        with open(kkrinputfilename,"w") as f:
            for line in writer:
                f.write( line +"\n") 
        #print kkrinputfilename,"is made."

if __name__ == "__main__": 

    print __name__
    filename=sys.argv[1]
    basename = os.path.basename(filename)
    filetype = basename.split(".")[1]
    print "filetype=",filetype

    headname,tailname = os.path.split(filename)
    datafilename = tailname.split(".")[0]

    inputparser = InputParser(datafilename)
    if filetype=="litecif":
        inputparser = inputparser.from_liteciffile(filename)
    elif filetype=="cif":
        inputparser = inputparser.from_ciffile(filename)

    kkrinputfile = "kkrinput"
    inputparser.write_file(kkrinputfile)
    print kkrinputfile,"is made."


