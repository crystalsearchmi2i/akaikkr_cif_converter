# -*- coding: utf-8 -*-

# for python2

# Copyright 2017 Hiori Kino, 
# This software includes the work that is distributed in the Apache License 2.0.


import sys
import CifFile
import copy
import pymatgen.io.cif
from pymatgen.io.vasp import Poscar

#from pymatgen.core.periodic_table import Element
import pymatgen.core.periodic_table 

import numpy as np


# cif labels
cif_singlelabel=["_chemical_formula_sum",
      "_space_group_crystal_system",
      "_symmetry_space_group_name_H-M",
      "_symmetry_Int_Tables_number" ,
      "_cell_length_a",
      "_cell_length_b",
      "_cell_length_c",
      "_cell_angle_alpha",
      "_cell_angle_beta",
      "_cell_angle_gamma",
      "_cell_volume",
      "_cell_formula_units_Z" ]

cif_symlabel = [ 
   "_symmetry_equiv_pos_site_id", 
   "_symmetry_equiv_pos_as_xyz" ]

cif_atomlabel = [
    "_atom_site_label",
    "_atom_site_fract_x",
    "_atom_site_fract_y",
    "_atom_site_fract_z",
    "_atom_site_occupancy",
    "_atom_site_symmetry_multiplicity",
    "_atom_site_Wyckoff_symbol",
    "_atom_site_type_symbol"]


def CifWriter(key,dic,ids):
    """
     input : 
         dic: cif のdic 表現
         ids = a list of labels to get in the loop_ part
     output : 
         Cif format の string list 
    """
    def string_conv(str):
        if str.count(" ")>0:
             return "'"+str+"'"
        return str

    lines = ["data_"+key]
    lines.append("")
    for n in cif_singlelabel:
        lines.append(" ".join([ n, string_conv(dic[n]) ] ) )

    loopspc="    "
    lines.append("")
    lines.append("loop_")
    for n in cif_symlabel:
        lines.append(loopspc+n)
    l=cif_symlabel[0]
    for i in range( len( dic[l] ) ):
        strlist = []
        for n in cif_symlabel:
            strlist.append( dic[n][i] )
        lines.append(" ".join(strlist))
    
    lines.append("")
    lines.append("loop_")
    for n in cif_atomlabel:
        lines.append(loopspc+n)
    l=cif_atomlabel[0]
    elem = pymatgen.core.periodic_table.Element("H")
    for i in range( len( dic[l] ) ):
            label =  elem.from_Z( ids[i] ).__str__()
            strlist = []
            for n in cif_atomlabel:
                if n=="_atom_site_label":
                    strlist.append( label )
                elif n=="_atom_site_type_symbol":
                    strlist.append( label )
                else:
                    strlist.append( dic[n][i] )
            #print strlist 
            lines.append(" ".join(strlist))

    lines.append("")
    return lines

class Atom():
    """ 
      information of each Atom
    """
    def __init__(self,label_wy, label, abc,xyz,element):
        """ 
        label_wy : e.g., element+wy, to specify element type indetail
        label = cif label
        abc = fractional coordinate
        xyz = cartedian coordianate
        element = cif lement
        number = Z of the element
        """
            
        self.label_wy=label_wy
        self.label=label
        self.abc=abc
        self.xyz=xyz
        self.element=element
        self.number=pymatgen.core.periodic_table.Element(element).number

    def __str__(self):
        s = [ self.label_wy, self.label, self.element, str(self.number) ,self.abc.__str__(), self.xyz.__str__() ]
        return " , ".join(s)
        
        
class Structure():
    """ 
       information of structure
       lattice
       atomic postions 
    """
    def __init__(self):
        """
        a,b,c,alpha,beta,gamma = crystal parameter
        tv = usually primitive vector
        atomkinds = atom list 
        spcid = spacegroup number 
        latticetpe = lattice type, e.g., fcc, bcc,...
        """
        self.a=0
        self.b=0
        self.c=0
        self.alpha=0
        self.beta=0
        self.gamma=0
        self.tv=[]
        self.atomkinds=[]
        self.spcid = 0
        self.latticetype = ""

    def get_label_wy_list(self):
        """ 
           wyckoff位置を含めたatomのlabelを返す
        """
        e = []
        for kinds in self.atomkinds:
             e.append(kinds[0].label_wy)
        return list(set(e))

    def get_element_list(self):
        """ 
        重複がないcifのelement listを返す.
        """
        e = []
        for kinds in self.atomkinds:
             e.append(kinds[0].element)
        return list(set(e))

    def get_number_list(self):
        """
        重複がない原子番号listを返す
        """
        e = []
        for kinds in self.atomkinds:
             e.append(kinds[0].number)
        return list(set(e))


    def __str__(self):
        strlist = []
        #print [self.a,self.b,self.c,self.alpha,self.beta,self.gamma]
        s = map(str, [ self.spcid, self.latticetype, self.akaikkr_latticetype ]  )
        strlist.append("spcid,latticetype="+ " ".join(s) )
        s = map(str, self.latticeuniqvar)
        strlist.append("latticeuniqvar="+" ".join(s) )
        s= map(str,[self.a,self.b,self.c,self.alpha,self.beta,self.gamma]) 
        strlist.append("a,b,c,alpha,beta,gamma= "+" ".join(s) )
        strlist.append("tv=")
        for v in self.tv:
           strlist.append( v.__str__() )
        strlist.append("atom=")
        for v in self.atomkinds:
           for s in v:
               strlist.append( s.__str__() )
        return "\n".join(strlist)

    def xsf_writer(self):
        """
          xsf 出力をstring listで返す
        """
        strlist= ["CRYSTAL","PRIMVEC"]
        for v in self.tv:
            strlist.append( " ".join(map(str,v)) )
        strlist.append("PRIMCOORD")
        n = 0
        for k in self.atomkinds:
           n += len(k)    
        strlist.append( " ".join( [str(n), "1"] ) )
        for k in self.atomkinds:
            for a in k:
               p = map( str, a.xyz )
               strlist.append(" ".join( [ a.label, p[0],p[1],p[2] ] ) )
        return strlist

def readcif(filename):
            """
               PyCifRW を用いてcifの情報を読む
               input: filename
               output dic形式 

               注意：
               pymatgenのcif libraryでは同じことができない。
            """
            cf = CifFile.ReadCif(filename)
            keys = cf.keys()
            fb = cf.first_block()
            dic = {}
            for n in cif_singlelabel:
                dic[n]=fb[n]
            for n in cif_symlabel:
                dic[n]=fb[n]
            for n in cif_atomlabel:
                dic[n]=fb[n]
            return keys,dic


class StructureParser():
    """
    実際はこれを呼ぶ
　　cif fileから 'element_Wyckoff座標'名を含めた全原子の情報を返す。
    """
    def __init__(self,filename):
        newciffile="__newstruc__.cif"
        # pymatgenを用いる
        # 残念ながら、label情報は捨てられるのでlabelを付け直す。
        # 一度新たなelement名でnewciffileをつくり
        self.WriteTempCif(filename, newciffile )
        # そのelement名でpymatgenを呼び、最後にelement名ー＞label名と付け直す
        self.ReadNewCif( newciffile )

    def WriteTempCif(self,filename,newciffile):
        """
         input: filename, newciffile 
         output: newciffileを作成

         element名を付け直してfile出力
        """
        keys,dic=readcif(filename)

        spcid= dic["_symmetry_Int_Tables_number"]

# process each element, then merge then
        pydiclist=[]
        labellist=dic["_atom_site_label"] 

        label_list = [] 
        id_list = []
        for i in range( len(labellist) ):
            label = "_".join( [dic["_atom_site_type_symbol"][i], dic["_atom_site_symmetry_multiplicity"][i]+ dic["_atom_site_Wyckoff_symbol"][i] , str(i+1) ] )
            label_list.append(label)
            id_list.append( i+1 )
        self.label_list = label_list
        self.id_list = id_list 

        writerlines = CifWriter(keys[0],dic,id_list)
        with open(newciffile,"w") as f:
            for n in writerlines:
                f.write(n+"\n")

    def ReadNewCif(self,newciffile):
        """ input: newciffile

            pymatgen.CifParserに従うが、
            output:
            self.dic_lattice  <- py.struc["lattice"]
            self.dic_sites_fix_list  <- py.struc["sites"] 
            ただし、element名をelement_multipliticyWyckoffcharacter_indexに付け直している。
        """
        py = pymatgen.io.cif.CifParser(newciffile)
        struc = py.get_structures()[0]
        dic_all= struc.as_dict()
        dic_lattice = dic_all["lattice"]
        dic_sites = dic_all["sites"]
        #print dic_lattice
        elem = pymatgen.core.periodic_table.Element("H")
        dic_sites_fix_list =  []
        for atom in  dic_sites:
            #print atom
            dic_sites_fix = {}
            for n in atom:
                if n=="label":
                    id_ = int(pymatgen.core.periodic_table.Element( atom[n] ).Z) -1
                    dic_sites_fix[n] = self.label_list[id_]
                else:   
                    #print "n=",n
                    dic_sites_fix[n] = atom[n]
            dic_sites_fix_list.append( dic_sites_fix )


        self.dic_lattice = dic_lattice
        self.dic_sites_fix_list = dic_sites_fix_list

        #print "----------------------------"
        #print dic_lattice
        #for i,atom in  enumerate(dic_sites_fix_list ):
        #    print i,atom

    def xsfWriter(self):
        """
          xsf 出力をstring listで返す
        """
        strlist= ["CRYSTAL","PRIMVEC"]
        for v in self.dic_lattice["matrix"]:
            strlist.append( " ".join(map(str,v)) )
        strlist.append("PRIMCOORD")
        n =  len ( self.dic_sites_fix_list ) 
        strlist.append( " ".join( [str(n), "1"] ) )
        for atom in self.dic_sites_fix_list:
               v1 = atom["xyz"]
               v1 = map(str,v1)
               v = [ atom["label"].split("_")[0] ]
               v.extend(v1)
               strlist.append(" ".join(  v ) )
        return strlist

    def poscarWriter(self):
        """ postcar 出力を返す
        """
        strlist = [ "comment", "1.0" ]
        for v in self.dic_lattice["matrix"]:
            strlist.append( " ".join(map(str,v)) )
        element_list = []
        for atom in self.dic_sites_fix_list:
            v =  atom["label"].split("_")[0] 
            element_list.append(v)
        element_list = list(set( element_list ))
        element_number = np.zeros( len ( element_list ) )

        for atom in self.dic_sites_fix_list:
            v =  atom["label"].split("_")[0]
            #print v
            for i,e in enumerate(element_list):
                if v == e:
                     element_number[i] += 1
                     break
        if np.sum(element_number) != len(self.dic_sites_fix_list):
            print "internal error , np.sum(element_number) != len(dic_sites_fix_list)"
            print self.dic_sites_fix_list
            print element_number
            raise Exception("internal error")

        strlist.append( " ".join(element_list) )
        strlist.append( " ".join( map(str,map(int,element_number)) ))
        strlist.append("CART")
        for atom in self.dic_sites_fix_list:
            v = copy.deepcopy(map(str,atom["xyz"]))
            v.append(  atom["label"].split("_")[0] )
            strlist.append(" ".join(v) )
        return strlist



def frac2cart(conv,v):
    """  conv = conventional vector
         v = fractional coordinate

        output: cartedian coordinate
    """
    conv = np.matrix(conv)
    v = np.array(v)
    return v*(conv)

def cfrac2pfrac(conv,prim,v):
    """ conv = conventional vector
         prim = primitive vector
         v = fractional coodinate as to the conventional vector
         output:   fractional coodinate as to the primitive vector 
    """
    cart =  frac2cart(conv,v)
    prim = np.matrix(prim)
    x =  np.linalg.solve(prim.T, cart.T).T
    return x 


if __name__ == "__main__": 

    filename=sys.argv[1]

    sp = StructureParser(filename)

    lines = sp.poscarWriter()
    with open("POSCAR","w") as f:
        for line in lines:
            f.write(line+"\n") 



